/*
+LU: 99/19
+Apellido: Krause Arnim
+Nombre: Francisco
*/
#include <iostream>
#include <list>
#include <map>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
    // ene, feb, mar, abr, may, jun
    31, 28, 31, 30, 31, 30,
    // jul, ago, sep, oct, nov, dic
    31, 31, 30, 31, 30, 31
};
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10
// Clase Fecha
class Fecha {
    public:
        Fecha (int mes, int dia):_mes(mes),_dia(dia){};
        int mes(){return this->_mes;}
        int dia(){return this->_dia;}
        void incrementar_dia();
        bool operator==(Fecha o);
        bool operator<(Fecha f);
    private:
        int _mes;
        int _dia;
};

ostream& operator<<(ostream& os, Fecha fecha) {
    os << fecha.dia() << "/"<< fecha.mes();
    return os;
}

bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}
//Defino este operador para poder usar a Fecha como key de un map.
bool operator < (Fecha f0, Fecha f1){
    return (f1.mes()==f0.mes() && f1.dia()>f0.dia()) || (f1.mes()>f0.mes());
}

void Fecha::incrementar_dia(){
    if(_dia+1>dias_en_mes(_mes)){
        _mes++;
        _dia = 1;
    } else {
        _dia++;
    }
}
// Ejercicio 11, 12
// Clase Horario
class Horario{
    public:
        Horario(uint hora, uint min):_hora(hora),_min(min){};
        uint hora(){return this->_hora;}
        uint min(){return this->_min;}
        bool operator<(Horario h);
    private:
        uint _hora;
        uint _min;
};

ostream& operator<<(ostream& os, Horario horario) {
    os << horario.hora() << ":"<< horario.min();
    return os;
}

bool Horario::operator<(Horario h){
    return (this->_hora<h.hora()) ||
        (this->_hora==h.hora() && this->_min<h.min());
}
// Ejercicio 13
// Clase Recordatorio
class Recordatorio{
    public:
        Recordatorio(Fecha fecha,Horario hora, string msj):_msj(msj),
                                                           _fecha(fecha),
                                                           _hora(hora){};
        string msj(){return this->_msj;}
        Fecha fecha(){return this->_fecha;}
        Horario hora(){return this->_hora;}
        bool operator<(Recordatorio rec){return this->_hora<rec.hora();}
    private:
        string _msj;
        Fecha _fecha;
        Horario _hora;
};
ostream& operator<<(ostream& os, Recordatorio record) {
    os << record.msj() << " @ "<< record.fecha() << " " << record.hora();
    return os;
}
// Ejercicio 14
// Clase Agenda
class Agenda{
    public:
        Agenda(Fecha fecha_inicial):_fecha_inicial(fecha_inicial){};
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        map<Fecha,list<Recordatorio>> recordatorios(){return this->_recordatorios;}
        Fecha hoy(){return this->_fecha_actual;}
    private:
        Fecha _fecha_inicial;
        Fecha _fecha_actual = _fecha_inicial;
        map<Fecha,list<Recordatorio>>_recordatorios;
};

void Agenda::agregar_recordatorio(Recordatorio rec){
    this->_recordatorios[rec.fecha()].push_back(rec);
    this->_recordatorios[rec.fecha()].sort();
}

void Agenda::incrementar_dia(){
    this->_recordatorios[_fecha_actual]={};
    this->_fecha_actual.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy(){
    return this->_recordatorios[_fecha_actual];
}

ostream& operator<<(ostream& os, Agenda ag){
    list<Recordatorio> los_de_hoy = ag.recordatorios_de_hoy();
    os << ag.hoy() << "\n";
    os << "=====" << "\n";

    for(Recordatorio rec : los_de_hoy){
        os << rec << "\n";
    }

    return os;
}
