#ifndef CONJUNTO_H_
#define CONJUNTO_H_

#include <assert.h>
#include <iostream>
#include <string>

using namespace std;

template <class T> class Conjunto {
  public:
    // Constructor. Genera un conjunto vacío.
    Conjunto();

    // Destructor. Debe dejar limpia la memoria.
    ~Conjunto();

    // Inserta un elemento en el conjunto. Si este ya existe,
    // el conjunto no se modifica.
    void insertar(const T &);

    // Decide si un elemento pertenece al conjunto o no.
    bool pertenece(const T &) const;

    // Borra un elemento del conjunto. Si este no existe,
    // el conjunto no se modifica.
    void remover(const T &);

    // Siguiente elemento al recibido por párametro, en orden.
    const T &siguiente(const T &elem);

    // Devuelve el mínimo elemento del conjunto según <.
    const T &minimo() const;

    // Devuelve el máximo elemento del conjunto según <.
    const T &maximo() const;

    // Devuelve la cantidad de elementos que tiene el conjunto.
    unsigned int cardinal() const;

    // Muestra el conjunto.
    void mostrar(std::ostream &) const;

  private:
    unsigned int _cardinal;
    struct Nodo {
        // El constructor, toma el elemento al que representa el nodo.
        Nodo(const T &v) : valor(v), izq(nullptr), der(nullptr), pad(nullptr){};
        // El elemento al que representa el nodo.
        T valor;
        // Puntero a la raíz del subárbol izquierdo.
        Nodo *izq;
        // Puntero a la raíz del subárbol derecho.
        Nodo *der;
        // Puntero al padre
        Nodo *pad;
    };
    // Puntero a la raíz de nuestro árbol.
    Nodo *_raiz;
    // Asumo raíz no null.
    Nodo *buscar(const T &clave) const {
        Nodo *actual_ptr = _raiz;
        bool encontrado = (_raiz != nullptr && clave == _raiz->valor);
        while (!(encontrado) && actual_ptr != nullptr) {
            //¿El nodo en el que estoy parado tiene el valor que busco?
            encontrado = clave == actual_ptr->valor;
            if (actual_ptr->valor > clave) {
                // encontrado = actual_ptr->der != nullptr &&
                //              actual_ptr->der->valor == clave;
                actual_ptr = actual_ptr->izq;
            } else if (actual_ptr->valor < clave) {
                // encontrado = actual_ptr->izq != nullptr &&
                //              actual_ptr->izq->valor == clave;
                actual_ptr = actual_ptr->der;
            }
        }
        return actual_ptr;
    }
    void trasplantar(Nodo *u, Nodo *v) {
        if (u->pad == nullptr) {
            _raiz = v;
        } else if (u == u->pad->izq) {
            u->pad->izq = v;
        } else {
            u->pad->der = v;
        }
        if (v != nullptr) {
            v->pad = u->pad;
        }
    }
    Nodo *minimo_desde(Nodo *inicio) const {
        bool izq_no_nil = inicio->izq != nullptr;
        while (izq_no_nil) {
            inicio = inicio->izq;
            izq_no_nil = inicio->izq != nullptr;
        }
        return inicio;
    }
    void borrar(Nodo *nodo) {
        if (nodo != nullptr) {
            borrar(nodo->der);
            borrar(nodo->izq);
            delete nodo;
        }
    }
};
template <class T>
std::ostream &operator<<(std::ostream &os, const Conjunto<T> &c) {
    c.mostrar(os);
    return os;
}

#include "Conjunto.hpp"

#endif // CONJUNTO_H_
