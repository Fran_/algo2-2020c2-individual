#include "Conjunto.h"
#include <cassert>
template <class T> Conjunto<T>::Conjunto() {
    _raiz = nullptr;
    _cardinal = 0;
}

template <class T> Conjunto<T>::~Conjunto() { borrar(_raiz); }

template <class T> bool Conjunto<T>::pertenece(const T &clave) const {
    Nodo *puntero_busqueda = buscar(clave);
    return (puntero_busqueda != nullptr && puntero_busqueda->valor == clave);
}
template <class T> void Conjunto<T>::insertar(const T &clave) {
    if (_raiz == nullptr) {
        Nodo *nuevo_nodo = new Nodo(clave);
        _raiz = nuevo_nodo;
        _raiz->izq = nullptr;
        _raiz->der = nullptr;
        _cardinal++;
    } else {
        if (!(pertenece(clave))) {
            bool insertado = false;
            Nodo *actual_ptr = _raiz;
            Nodo *izq = actual_ptr->izq;
            Nodo *der = actual_ptr->der;
            while (!insertado) {
                der = actual_ptr->der;
                izq = actual_ptr->izq;
                // Si la clave es mayor al nodo actual.
                if (actual_ptr->valor < clave) {
                    // Veo si el hijo derecho es nil,
                    // de serlo, y como la clave es mayor
                    // a valor, insertamos la clave,
                    // y hacemos que der apunte al nuevo hijo.
                    if (der == nullptr) {
                        Nodo *nuevo_nodo = new Nodo(clave);
                        actual_ptr->der = nuevo_nodo;
                        nuevo_nodo->pad = actual_ptr;
                        insertado = true;
                        _cardinal++;
                    } else {
                        // Si der no es null, es porque tenemos un valor
                        // ahí, entonces sigo el camino del hijo derecho.
                        actual_ptr = actual_ptr->der;
                    }
                }
                //Ídem derecha, pero con el hijo izquierdo.
                else {
                    if (izq == nullptr) {
                        Nodo *nuevo_nodo = new Nodo(clave);
                        actual_ptr->izq = nuevo_nodo;
                        nuevo_nodo->pad = actual_ptr;
                        insertado = true;
                        _cardinal++;
                    } else {
                        actual_ptr = actual_ptr->izq;
                    }
                }
            }
        }
    }
}

template <class T> void Conjunto<T>::remover(const T &clave) {
    // Busco la clave:
    Nodo *nodo_a_borrar = buscar(clave);
    if (nodo_a_borrar->izq == nullptr) {
        trasplantar(nodo_a_borrar, nodo_a_borrar->der);
        _cardinal--;
    } else if (nodo_a_borrar->der == nullptr) {
        trasplantar(nodo_a_borrar, nodo_a_borrar->izq);
        _cardinal--;
    } else {
        Nodo *sucesor = minimo_desde(nodo_a_borrar->der);
        if (sucesor->pad != nodo_a_borrar) {
            trasplantar(sucesor, sucesor->der);
            sucesor->der = nodo_a_borrar->der;
            sucesor->der->pad = sucesor;
        }
        trasplantar(nodo_a_borrar, sucesor);
        sucesor->izq = nodo_a_borrar->izq;
        sucesor->izq->pad = sucesor;
        _cardinal--;
    }
    delete nodo_a_borrar;
}

template <class T> const T &Conjunto<T>::siguiente(const T &clave) {
    Nodo *nodo_clave = buscar(clave);
    if (nodo_clave->der != nullptr) {
        Nodo *minimo_subarb_der = minimo_desde(nodo_clave->der);
        return minimo_subarb_der->valor;
    }
    Nodo *padre_nodo = nodo_clave->pad;
    while (padre_nodo != nullptr && nodo_clave == padre_nodo->der) {
        nodo_clave = padre_nodo;
        padre_nodo = padre_nodo->pad;
    }
    return padre_nodo->valor;
}

template <class T> const T &Conjunto<T>::minimo() const {
    Nodo *nodo_minimo = minimo_desde(_raiz);
    return nodo_minimo->valor;
}

template <class T> const T &Conjunto<T>::maximo() const {
    // assert(false);
    Nodo *ptr_buscador = _raiz;
    bool der_no_nil = _raiz->der != nullptr;
    while (der_no_nil) {
        ptr_buscador = ptr_buscador->der;
        der_no_nil = ptr_buscador->der != nullptr;
    }
    return ptr_buscador->valor;
}

template <class T> unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T> void Conjunto<T>::mostrar(std::ostream &) const {
    assert(false);
}
