#include "Lista.h"
Lista::Lista() {
    this->tam_ = 0;
    this->primero_ = nullptr;
    this->ultimo_ = nullptr;
}

Lista::Lista(const Lista &l) : Lista() {
    // Inicializa una lista vacía y luego utiliza operator= para no duplicar el
    // código de la copia de una lista.
    *this = l;
}


Lista::~Lista() {
    Nodo* actual_ptr = primero_;
    while (actual_ptr != nullptr) {
        actual_ptr = actual_ptr->siguiente;
        delete primero_;
        primero_ = actual_ptr;
    }
}
Lista &Lista::operator=(const Lista &aCopiar) {
    // Completar
    int i = this->tam_;
    for (int j = 0; j < i; j++) {
        eliminar(0);
    }
    Nodo* actual_ptr = aCopiar.primero_;
    while (actual_ptr != nullptr) {
        agregarAtras(actual_ptr->valor);
        actual_ptr=actual_ptr->siguiente;
    }
    return *this;
}

// Si el actual primer nodo existe, entonces no
// ha de tener puntero null, con lo que al siguiente
// que debe de apuntar el que será el primero es al
// primero antes de agregar elem adelante.
void Lista::agregarAdelante(const int &elem) {
    // Completar
    // Un nuevo primer nodo
    Nodo *nuevo_ptr = new Nodo();
    nuevo_ptr->valor = elem;
    nuevo_ptr->siguiente = nullptr;
    nuevo_ptr->anterior = nullptr;
    if (tam_ == 0) {
        ultimo_ = nuevo_ptr;
    } else {
        primero_->anterior = nuevo_ptr;
        nuevo_ptr->siguiente = primero_;
    }
    tam_++;
    primero_ = nuevo_ptr;
}

void Lista::agregarAtras(const int &elem) {
    if (tam_ == 0) {
        agregarAdelante(elem);
    } else {
        Nodo *nuevo_ptr = new Nodo();
        nuevo_ptr->valor = elem;
        nuevo_ptr->anterior = ultimo_;
        ultimo_->siguiente = nuevo_ptr;
        nuevo_ptr->siguiente = nullptr;
        ultimo_ = nuevo_ptr;
        tam_++;
    }
}

void Lista::eliminar(Nat i) {
    int k = 0;
    Nodo *actual_ptr = primero_;
    if (tam_ == 1) {
        primero_ = nullptr;
        ultimo_ = nullptr;
    } else {
        while (k < i) {
            actual_ptr = actual_ptr->siguiente;
            k++;
        }
        if (k == 0) {
            actual_ptr->siguiente->anterior = nullptr;
            primero_ = actual_ptr->siguiente;
        } else if (k == tam_ - 1) {
            actual_ptr->anterior->siguiente = nullptr;
            ultimo_ = actual_ptr->anterior;
        } else {
            actual_ptr->anterior->siguiente = actual_ptr->siguiente;
            actual_ptr->siguiente->anterior = actual_ptr->anterior;
        }
    }
    tam_--;
    delete actual_ptr;
}

int Lista::longitud() const { return tam_; }

// Voy a asumir que nos tiran un i en rango.
const int &Lista::iesimo(Nat i) const {
    int k = 0;
    Nodo *actual_ptr = primero_;
    while (k < i) {
        actual_ptr = actual_ptr->siguiente;
        k++;
    }
    return actual_ptr->valor;
}

int &Lista::iesimo(Nat i) {
    int k = 0;
    Nodo *actual_ptr = primero_;
    while (k < i) {
        actual_ptr = actual_ptr->siguiente;
        k++;
    }
    return actual_ptr->valor;
}

void Lista::mostrar(ostream &o) {
    Nodo* actual_ptr = primero_;
    o << "[";
    while (actual_ptr != nullptr) {
        o<< actual_ptr->valor << ", ";
        actual_ptr = actual_ptr->siguiente;
    }
    o << "]"<<endl;
}
