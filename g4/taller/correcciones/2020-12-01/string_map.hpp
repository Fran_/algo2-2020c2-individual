template <typename T> string_map<T>::string_map() {
    raiz = new Nodo();
    raiz->definicion = nullptr;
    _size = 0;
}

template <typename T>
string_map<T>::string_map(const string_map<T> &aCopiar) : string_map() {
    *this = aCopiar;
} // Provisto por la catedra: utiliza el operador asignacion para realizar la
  // copia.

template <typename T>
string_map<T> &string_map<T>::operator=(const string_map<T> &d) {
    if (raiz != nullptr) {
        vector<Nodo *> nulls(256, nullptr);
        while (raiz->siguientes != nulls) {
            Nodo *flecha = raiz;
            int i = 0;
            Nodo *anterior;
            int letra;
            while (flecha->siguientes != nulls) {
                i = 0;
                while (i < 256) {
                    if (flecha->siguientes[i] != nullptr) {
                        anterior = flecha;
                        letra = i;
                        flecha = flecha->siguientes[i];
                        i = 256;
                    } else {
                        i++;
                    }
                }
            }
            anterior->siguientes[letra] = nullptr;
            if (flecha->definicion != nullptr) {

                delete flecha->definicion;
            }
            delete flecha;
        }
        delete raiz;
        raiz = nullptr;
        _size = 0;
        _claves = {};
    }
    list<pair<string, T>> nuevas_claves = d.claves;
    for (pair<string, T> par : nuevas_claves) {
        insert(par);
    }
    return *this;
}

template <typename T> string_map<T>::~string_map() {
    if (raiz != nullptr) {
        vector<Nodo *> nulls(256, nullptr);
        while (raiz->siguientes != nulls) {
            Nodo *flecha = raiz;
            int i = 0;
            Nodo *anterior;
            int letra;
            while (flecha->siguientes != nulls) {
                i = 0;
                while (i < 256) {
                    if (flecha->siguientes[i] != nullptr) {
                        anterior = flecha;
                        letra = i;
                        flecha = flecha->siguientes[i];
                        i = 256;
                    } else {
                        i++;
                    }
                }
            }
            anterior->siguientes[letra] = nullptr;
            if (flecha->definicion != nullptr) {
                delete flecha->definicion;
            }
            delete flecha;
        }
        delete raiz->definicion;
        raiz->definicion = nullptr;
        delete raiz;
        raiz = nullptr;
    }
}

template <typename T> T &string_map<T>::operator[](const string &clave) {
    if (!count(clave)) {
        insert(make_pair(clave, T()));
    }
    return at(clave);
}

template <typename T> int string_map<T>::count(const string &clave) const {
    Nodo *flecha = raiz;
    if (raiz == nullptr)
        return 0;
    int valor_letra;
    for (char letra : clave) {
        valor_letra = int(letra);
        if (flecha->siguientes[valor_letra] == nullptr)
            return 0;
        flecha = flecha->siguientes[valor_letra];
    }
    return (flecha->definicion != nullptr);
}

template <typename T> const T &string_map<T>::at(const string &clave) const {
    Nodo *x = obtener(raiz, clave, 0);
    return *x->definicion;
}

template <typename T> T &string_map<T>::at(const string &clave) {
    Nodo *x = obtener(raiz, clave, 0);
    return *x->definicion;
}
template <typename T>
bool misma_clave(pair<string, T> &clave_lista, const string &clave) {
    return (clave == clave_lista.first);
}
template <typename T> void string_map<T>::erase(const string &clave) {
    // Iterador para borrar la clave de la lista.
    typename list<pair<string, T>>::iterator it = claves.begin();
    while (it != claves.end()) {
        if ((*it).first == clave) {
            // Usar erase avanza el iterador.
            it = claves.erase(it);
        } else {
            it++;
        }
    }
    raiz = borrar(raiz, clave, 0);
    _size--;
}

template <typename T> int string_map<T>::size() const { return _size; }

template <typename T> bool string_map<T>::empty() const { return _size == 0; }

template <typename T> void string_map<T>::insert(const pair<string, T> &par) {
    claves.push_back(par);
    raiz = insertar(raiz, par, 0);
    _size++;
}
