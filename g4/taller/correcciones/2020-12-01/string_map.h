#ifndef STRING_MAP_H_
#define STRING_MAP_H_

#include <list>
#include <string>
#include <vector>

using namespace std;

template <typename T> class string_map {
  public:
    /**
    CONSTRUCTOR
    * Construye un diccionario vacio.
    **/
    string_map();

    /**
    CONSTRUCTOR POR COPIA
    * Construye un diccionario por copia.
    **/
    string_map(const string_map<T> &aCopiar);

    /**
    OPERADOR ASIGNACION
     */
    string_map &operator=(const string_map &d);

    /**
    DESTRUCTOR
    **/
    ~string_map();

    /**
    INSERT
    * Inserta un par clave, valor en el diccionario
    **/
    void insert(const pair<string, T> &);

    /**
    COUNT
    * Devuelve la cantidad de apariciones de la clave (0 o 1).
    * Sirve para identificar si una clave está definida o no.
    **/

    int count(const string &key) const;

    /**
    AT
    * Dada una clave, devuelve su significado.
    * PRE: La clave está definida.
    --PRODUCE ALIASING--
    -- Versión modificable y no modificable
    **/
    const T &at(const string &key) const;
    T &at(const string &key);

    /**
    ERASE
    * Dada una clave, la borra del diccionario junto a su significado.
    * PRE: La clave está definida.
    --PRODUCE ALIASING--
    **/
    void erase(const string &key);

    /**
     SIZE
     * Devuelve cantidad de claves definidas */
    int size() const;

    /**
     EMPTY
     * devuelve true si no hay ningún elemento en el diccionario */
    bool empty() const;

    /** OPTATIVO
     * operator[]
     * Acceso o definición de pares clave/valor
     **/
    T &operator[](const string &key);

  private:
    struct Nodo {
        vector<Nodo *> siguientes;
        T *definicion;
        Nodo() : siguientes(256, nullptr), definicion(nullptr){};
    };
    Nodo *raiz;
    int _size;
    vector<pair<string, T>> _claves;
    Nodo *insertar(Nodo *x, pair<string, T> par, int d) {
        if (x == nullptr) {
            x = new Nodo();
        }
        if (d == par.first.size()) {
            T *def = new T;
            *def = par.second;
            delete x->definicion;
            x->definicion = def;
            return x;
        }
        char c = par.first[d];
        x->siguientes[c] = insertar(x->siguientes[c], par, d + 1);
        return x;
    }
    Nodo *obtener(Nodo *x, string clave, int d) {
        if (d == clave.size()) {
            return x;
        }
        char c = clave[d];
        return obtener(x->siguientes[c], clave, d + 1);
    }
    int buscar(Nodo *x, string s, int d, int largo) {
        if (x == nullptr) {
            return largo;
        }
        if (x->definicion != nullptr) {
            largo = d;
        }
        if (d == s.size()) {
            return largo;
        }
        char c = s[d];
        return buscar(x->siguientes[c], s, d + 1, largo);
    }
    Nodo *borrar(Nodo *x, string clave, int d) {
        if (x == nullptr) {
            return nullptr;
        }
        if (d == clave.size()) {
            delete x->definicion;
            x->definicion = nullptr;
        } else {
            char c = clave[d];
            x->siguientes[c] = borrar(x->siguientes[c], clave, d + 1);
        }
        if (x->definicion != nullptr)
            return x;
        for (Nodo *letra : x->siguientes) {
            if (letra == nullptr) {
                return x;
            }
        }
        return nullptr;
    }
    list<pair<string, T>> claves;
};

#include "string_map.hpp"

#endif // STRING_MAP_H_
